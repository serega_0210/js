
      
        //создание функции с помощью FunctionDeclaration(Именованые)
        function myFirstFunction () {
            document.write("<p>Hello from MyFirstFunction!");
            document.write("<p>How are you?");
            document.write("<p>Goodbye!");
        };

        function mySecondFunction() {
            document.write("<p>Hello from MySecondFunction!");
        }
        // Вызов функций.

        myFirstFunction();

        document.write("<hr/>");

        // Вызов функций.
        mySecondFunction();

        document.write("<hr/>");

        //console.dir(window)

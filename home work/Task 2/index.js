//Использую циклы нарисуйте в браузере с помощью пробелов () и звездочек (): Прямоугольник, Прямоугольный треугольник, Равносторонний  треуголник, Ромб 

// Прямоугольник
let size = 20;

for (let i = 0; i < size; i++) {
    for (let j = 0; j < size; j++) {
        document.write("*&nbsp");
    }

    document.write("<br>");
}
// Отступ
for (let i = 0; i < 4; i++) {
    document.write("<br>");
}
// Прямоугольный треугольник
for (let i = 0; i < size; i++) {
    for (let j = size - (i + 1); j < size; j++) {
        document.write("*&nbsp");
    }

    document.write("<br>");
}
// Отступ
for (let i = 0; i < 4; i++) {
    document.write("<br>");
}

// Равносторонний треугольник
for (let i = 0; i < size; i++) {

    const side = i * 2 + 1;

    for (let j = side; j < size * 2; j++) {
        document.write("&nbsp");
    }

    for (let k = side; k > 0; k--) {
        document.write("*");
    }

    document.write("<br>");
}
// Отступ
for (let i = 0; i < 4; i++) {
    document.write("<br>");
}

// Ромб
for (let i = 0; i < size - 1; i++) {

    const side = i * 2 + 1;

    for (let j = side; j < size * 2; j++) {
        document.write("&nbsp");
    }

    for (let z = side; z > 0; z--) {
        document.write("*");
    }

    document.write("<br>");
}

for (let i = 0; i < size; i++) {
    const side = i * 2 + 1;

    for (let j = side; j > 0; j--) {
        document.write("&nbsp");
    }

    for (let z = 0; z < size * 2 - side; z++) {
        document.write("*");
    }

    document.write("<br>");
}